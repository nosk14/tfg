﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.FRT;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuManager : MonoBehaviour
{
    public readonly Color BASE_COLOR = new Color(194f / 255, 194f / 255, 194f / 255);
    public readonly Color MARKED_COLOR = new Color(146f / 255, 146f / 255, 146f / 255);
    public Image[] genderImages;
    public Image[] ageImages;
    public GameObject ARuser;

    public GameObject scorePanel;
    public GameObject menu;
    public GameObject result;

    public void chooseGender(int g)
    {
        foreach (Image i in genderImages) i.color = BASE_COLOR;

        EventSystem.current.currentSelectedGameObject.gameObject.GetComponent<Image>().color = MARKED_COLOR;

        FRTData.gender = (Gender)g;
    }

    public void chooseAge(int a)
    {
        foreach (Image i in ageImages) i.color = BASE_COLOR;

        EventSystem.current.currentSelectedGameObject.gameObject.GetComponent<Image>().color = MARKED_COLOR;

        FRTData.ageRange = (AgeRange)a;
    }

    public void confirmData()
    {
        if (FRTData.ageRange != null && FRTData.gender != null)
        {
            menu.SetActive(false);
            Cursor.visible = false;
            ARuser.SetActive(true);
            scorePanel.SetActive(true);
            FRTData.canPlay = true;
        }
    }

    public void exit()
    {
        Application.Quit();
    }

    public void changeData()
    {
        result.SetActive(false);
        menu.SetActive(true);
    }

    public void continueTest()
    {
        Cursor.visible = false;
        ARuser.SetActive(true);
        scorePanel.SetActive(true);
        result.SetActive(false);
        FRTData.canPlay = true;
    }

}
