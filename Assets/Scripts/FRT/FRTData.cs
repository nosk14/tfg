﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.FRT
{
    public static class FRTData
    {
        private static readonly Dictionary<Gender, Dictionary<AgeRange, float>> scoreTable =
            new Dictionary<Gender, Dictionary<AgeRange, float>>()
            {
                {
                    Gender.MALE, new Dictionary<AgeRange, float>()
                    {
                        { AgeRange.R19_LESS, 0.42f },
                        { AgeRange.R20_40, 0.42f },
                        { AgeRange.R41_69, 0.37f },
                        { AgeRange.R70_MORE, 0.33f }
                    }
                },
                {
                    Gender.FEMALE, new Dictionary<AgeRange, float>()
                    {
                        { AgeRange.R19_LESS, 0.37f },
                        { AgeRange.R20_40, 0.37f },
                        { AgeRange.R41_69, 0.35f },
                        { AgeRange.R70_MORE, 0.26f }
                    }
                }
            };

        public static Gender? gender;
        public static AgeRange? ageRange;
        public static bool canPlay = false;

        public static float getExpectedResult()
        {
            return FRTData.scoreTable[(Gender)FRTData.gender][(AgeRange)FRTData.ageRange];
        }


    }
}
