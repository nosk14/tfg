﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using Assets.Scripts.FRT;
using System.Collections.Generic;

/*
    This class is the one that manages the states during the test with the gestures and provides the test results
 */

public class FRTGestureListener : MonoBehaviour, KinectGestures.GestureListenerInterface
{
    private KinectManager kinect;
    public static float FEET_MARGIN = 0.05f;

    // GUI Elements
    public Text scoreText;
    public GameObject resultWindow;
    public Text scoreResult;
    public Text shouldScore;
    public Text evaluation;
    public GameObject ARUser;

    // Milestone object
    public GameObject milestonePrefab;
    private GameObject milestone;

    // Data of the test while in progress
    //private List<Vector3> sampling = new List<Vector3>(); // samplings of hand position
    //private DateTime? poseStart; // When the user starts the test
    //private KinectGestures.Gestures status = KinectGestures.Gestures.FRT_InitialPose; // Current state of the test ( inital state )

    //private int handJoint;
    // private Vector3 leftFeed = Vector3.negativeInfinity;
    // private Vector3 rightfeed = Vector3.negativeInfinity;

    private float score = 0f; // distance from the hand at this moment to the inital position
    private bool testInProgress = false;
    private Vector3 initialHandPosition; // Position of the hand when the test starts
    private int handUsed;
    private Vector3 leftFoot; // Position of the hand when the test starts
    private Vector3 rightFoot; // Position of the hand when the test starts


    public void Start()
    {
        this.kinect = KinectManager.Instance;
    }

    public void Update()
    {
        if (testInProgress && kinect.IsUserDetected())
        {
            uint user = kinect.GetPlayer1ID();
            if (haveFeetMoved(user)){
                Cursor.visible = true; // Makes mouse visible 
                ARUser.SetActive(false); // Stops rendering user on the scene
                scoreText.gameObject.transform.parent.gameObject.SetActive(false); // Hides score label
                resultWindow.SetActive(true); // Shows result window
                configResultWindow();
                FRTData.canPlay = false; // Stops the test
                GameObject.Destroy(milestone);
            }
            else
            {
                score = Vector3.Distance(initialHandPosition, kinect.GetJointPosition(user, handUsed));
                scoreText.text = (score * 100).ToString("n2") + " cm";
            }
        }
    }

    public bool GestureCompleted(uint userId, int userIndex, KinectGestures.Gestures gesture, KinectWrapper.NuiSkeletonPositionIndex joint, Vector3 screenPos)
    {
        if (!FRTData.canPlay || testInProgress) return false;

        leftFoot = this.kinect.GetJointPosition(userId, (int)KinectWrapper.NuiSkeletonPositionIndex.FootLeft);
        rightFoot = this.kinect.GetJointPosition(userId, (int)KinectWrapper.NuiSkeletonPositionIndex.FootRight);

        handUsed = (int)joint;
        initialHandPosition = this.kinect.GetJointPosition(userId, (int)joint);
        spawnMilestone(userId, joint);

        testInProgress = true;
        return true;
    }

    private void spawnMilestone(uint userId, KinectWrapper.NuiSkeletonPositionIndex handUsed)
    {
        KinectWrapper.NuiSkeletonPositionIndex shoulder =
            handUsed == KinectWrapper.NuiSkeletonPositionIndex.HandLeft ? 
                KinectWrapper.NuiSkeletonPositionIndex.ShoulderLeft : KinectWrapper.NuiSkeletonPositionIndex.ShoulderRight;

        Vector3 shoulderPos = kinect.GetJointPosition(userId, (int)shoulder);
        Vector3 handPos = kinect.GetJointPosition(userId, (int)handUsed);

        Vector3 spawnDirection = (handPos - shoulderPos).normalized;
        Vector3 spawnPosition = handPos + (spawnDirection * FRTData.getExpectedResult());
        spawnPosition.y = 0.528f;

        milestone = GameObject.Instantiate(milestonePrefab);
        milestone.transform.position = spawnPosition;
    }

    private bool haveFeetMoved(uint user)
    {
        Vector3 currentLeftFoot = kinect.GetJointPosition(user, (int)KinectWrapper.NuiSkeletonPositionIndex.FootLeft);
        if (Vector3.Distance(currentLeftFoot, leftFoot) > FEET_MARGIN) return false;

        Vector3 currenRightFoot = kinect.GetJointPosition(user, (int)KinectWrapper.NuiSkeletonPositionIndex.FootRight);
        if (Vector3.Distance(currenRightFoot, rightFoot) > FEET_MARGIN) return false;

        return true;
    }

    private void configResultWindow()
    {
        scoreResult.text = scoreText.text;
        float expected = FRTData.getExpectedResult();
        shouldScore.text = (expected * 100).ToString("n2") + " cm";

        if (score < 0.90 * expected)
        {
            evaluation.text = "Not good enought";
            evaluation.color = Color.yellow;
        }
        else if (score > 1.10 * expected)
        {
            evaluation.text = "Very good!";
            evaluation.color = Color.green;
        }
        else
        {
            evaluation.text = "Good";
            evaluation.color = Color.green;
        }
    }

    // Implemented by interface but unused
    #region unused
    public void UserDetected(uint userId, int userIndex)
    {
    }

    public void UserLost(uint userId, int userIndex)
    {
    }

    // Called when the gesture stops
    public bool GestureCancelled(uint userId, int userIndex, KinectGestures.Gestures gesture, KinectWrapper.NuiSkeletonPositionIndex joint)
    {
        return true;
    }

    // Called each frame while gesture is in progress
    public void GestureInProgress(uint userId, int userIndex, KinectGestures.Gestures gesture, float progress, KinectWrapper.NuiSkeletonPositionIndex joint, Vector3 screenPos)
    {

    }
    #endregion

}
