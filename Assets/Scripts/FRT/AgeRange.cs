﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.FRT
{
    public enum AgeRange
    {
        R19_LESS = 0,
        R20_40,
        R41_69,
        R70_MORE
    }
}
