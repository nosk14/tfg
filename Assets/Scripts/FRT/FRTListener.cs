﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using Assets.Scripts.FRT;


/*
    DEPRECATED
*/
public class FRTListener : MonoBehaviour, KinectGestures.GestureListenerInterface
{
    private bool wasInProgress = false;
    private Vector3 originalHandPosition;
    private DateTime? startPose = null;
    private float score;

    private KinectManager kinect;

    public Text scoreText;
    public GameObject resultWindow;
    public Text scoreResult;
    public Text shouldScore;
    public Text evaluation;
    public GameObject ARUser;


    public bool GestureCancelled(uint userId, int userIndex, KinectGestures.Gestures gesture, KinectWrapper.NuiSkeletonPositionIndex joint)
    {
        if (!FRTData.canPlay) return true;

        if ((DateTime.Now - (DateTime)startPose).TotalMilliseconds > 2000)
        {
            Cursor.visible = true;
            ARUser.SetActive(true);
            scoreText.gameObject.transform.parent.gameObject.SetActive(false);
            resultWindow.SetActive(true);
            configResultWindow();
            FRTData.canPlay = false;
        }

        startPose = null;
        wasInProgress = false;

        return true;
    }

    public void GestureInProgress(uint userId, int userIndex, KinectGestures.Gestures gesture, float progress, KinectWrapper.NuiSkeletonPositionIndex joint, Vector3 screenPos)
    {
        if (!FRTData.canPlay) return;

        kinect = KinectManager.Instance;

        // Wait for stable pose
        if (startPose == null) startPose = DateTime.Now;
        if ((DateTime.Now - (DateTime)startPose).TotalMilliseconds < 500) return;

        if (!wasInProgress)
        {
            originalHandPosition = kinect.GetJointPosition(userId, (int)joint);
        }

        Vector3 actualHandPos = kinect.GetJointPosition(userId, (int)joint);
        float distance = Vector3.Distance(originalHandPosition, actualHandPos);

        score = distance;
        scoreText.text = (distance * 100).ToString("n2") + " cm";

        wasInProgress = true;

    }

    private void configResultWindow()
    {
        resultWindow.SetActive(true);

        scoreResult.text = scoreText.text;
        float expected = FRTData.getExpectedResult();
        shouldScore.text = (expected * 100).ToString("n2") + " cm";

        if (score < 0.90 * expected)
        {
            evaluation.text = "Not good enought";
            evaluation.color = Color.yellow;
        }
        else if (score > 1.10 * expected)
        {
            evaluation.text = "Very good!";
            evaluation.color = Color.green;
        }
        else
        {
            evaluation.text = "Good";
            evaluation.color = Color.green;
        }
    }


    // Implemented by interface but unused
    #region unused
    public bool GestureCompleted(uint userId, int userIndex, KinectGestures.Gestures gesture, KinectWrapper.NuiSkeletonPositionIndex joint, Vector3 screenPos)
    {
        return true;
    }

    public void UserDetected(uint userId, int userIndex)
    {
    }

    public void UserLost(uint userId, int userIndex)
    {
    }
    #endregion

}
