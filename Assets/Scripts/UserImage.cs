using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/*
	This process is in charge of drawing the user in the 3d world.
	It gets the texture, sacles it according to the relative size and 
	moves it on the skeleton object.
*/
public class UserImage : MonoBehaviour
{

    private const float SCALE_CORRECTOR = 1f; // DEPRECATED //
    private KinectManager kinect;

    public SkeletonTraker skeletonTracker;
    private Skeleton skeleton;

    // Initialization
    void Start()
    {
		// get user texture and set it to this object ( object should be a plane )
        Texture2D text = KinectManager.Instance.GetUsersLblTex();
        this.GetComponent<Renderer>().material.mainTexture = text;
    }

    // Update de image position each frame
    void Update()
    {
        kinect = KinectManager.Instance;

        if (kinect && kinect.IsInitialized() && kinect.IsUserDetected())
        {
            uint player = kinect.GetPlayer1ID();
            skeleton = this.skeletonTracker.skeleton;
            scalePlane(player); // Set size of the image
            setPlanePosition(player); // Move it to the corresponding position in the 3d world

            // Width and height of the plane cant be 0, if it happends it must be scaled again 
            Vector3 extents = this.GetComponent<Renderer>().bounds.extents;
            if (extents.x == 0 || extents.z == 0)
            {
                this.transform.localScale = new Vector3(1f, 1f, 1f);
            }
        }
    }


	// This functions sacles this GameObject (the plane holding the user texture) to the size of
	// the user in the 3d world
    private void scalePlane(uint player)
    {
		// Head and Spine are the points of reference for scaling, so we need them
        if (kinect.IsJointTracked(player, (int)KinectWrapper.NuiSkeletonPositionIndex.Spine) && kinect.IsJointTracked(player, (int)KinectWrapper.NuiSkeletonPositionIndex.Head))
        {
            // Get joint positions relative to texture
            Vector2 colorPosA = getJointColorPosition(player, (int)KinectWrapper.NuiSkeletonPositionIndex.Spine);
            Vector3 refAWorld = colorToWorldCoordinates(colorPosA);

            Vector2 colorPosB = getJointColorPosition(player, (int)KinectWrapper.NuiSkeletonPositionIndex.Head);
            Vector3 refBWorld = colorToWorldCoordinates(colorPosB);

			// Get distance between two points on the texture
            float textureDist = Vector2.Distance(refAWorld, refBWorld);

            // Get joint positions in real world
            Vector3 skeletonPosA = this.skeleton.spine.transform.position;
            Vector3 skeletonPosB = this.skeleton.head.transform.position;

			// Get distance between two points on 3d world
            float skeletonDist = Vector3.Distance(skeletonPosA, skeletonPosB);

            
            if (textureDist != 0)
            {
                float scaleFactor = (skeletonDist / textureDist) * SCALE_CORRECTOR; // Compare the two distances to get the sacale factor of the texture
                this.transform.localScale = new Vector3(this.transform.localScale.x * scaleFactor, this.transform.localScale.y, this.transform.localScale.z * scaleFactor); // Scale the object
            }
        }
    }

	// Move the gameobject containing the texture to set with the 3d skeleton
    private void setPlanePosition(uint player)
    {
		// get spine position to use it as reference
        Vector3 skeletonReferencePosition = this.skeleton.spine.transform.position; // from skeleton
        Vector3 colorPos = getJointColorPosition(player, (int)KinectWrapper.NuiSkeletonPositionIndex.Spine); // from color map (texture)

		// calc the offset between the skeleton spine and the texture spine
        Vector3 textureWolrdPos = colorToWorldCoordinates(colorPos);
        Vector3 diff = this.transform.position - textureWolrdPos;

		// apply correction to that offset
        this.transform.position = skeletonReferencePosition + diff;
    }

	// Return the position of a joint in color map coordinates
    private Vector2 getJointColorPosition(uint playerID, int joint)
    {
        Vector3 jointPos = kinect.GetRawSkeletonJointPos(playerID, joint);
        Vector2 depthPos = kinect.GetDepthMapPosForJointPos(jointPos);
        Vector2 colorPos = kinect.GetColorMapPosForDepthPos(depthPos);

        return colorPos;
    }

	// Return the 3d world coordinates of the incoming point in the texture
    private Vector3 colorToWorldCoordinates(Vector2 pos)
    {
        // Relative position of color map
        Vector2 rPos = new Vector2(pos.x / (float)KinectWrapper.GetColorWidth(), pos.y / (float)KinectWrapper.GetColorHeight());

        // Get plane size
        Bounds b = this.GetComponent<Renderer>().bounds;
        Vector3 topLeft = new Vector3(b.center.x - b.extents.x,
                                      b.center.y + b.extents.y,
                                      b.center.z - b.extents.z); // calc top-left


        Vector3 worldpoint = new Vector3(topLeft.x + (rPos.x * b.extents.x * 2),
                                         topLeft.y - (rPos.y * b.extents.y * 2),
                                         topLeft.z);

        return worldpoint;

    }

}
