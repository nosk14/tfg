﻿using UnityEngine;
using System.Collections;

/* This Class is just for limit the framerate of the application
   to solve some visual problems in computer with high performance */

public class AppSpecs : MonoBehaviour {

    // this value is set in the inspector
    public int fps;


	void Start () {
        Application.targetFrameRate = fps;
	}
	
	void Update () {
	
	}
}
