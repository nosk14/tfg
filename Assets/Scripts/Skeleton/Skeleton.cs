﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Rendering;

/*
	This class contains all the information about the skeleton itself.
	All bones and their properties are defined and initialized here.
*/

public class Skeleton
{
    // Static properties of skeleton
    private const float BONE_RADIUS = 0.065f;
    private const float CHEST_WIDTH = 0.1f;
    private const float BONE_MASS = 1;
    private const float BONE_DRAG = 1;
    private const float BONE_ANGULAR_DRAG = 1f;
    private static Vector3 HAND_SCALE = new Vector3(0.07f, 0.05f, 0.02f);
    private const float HEAD_SCALE = 0.175f;
    public static string HAND_TAG = "hand";
	private int SkeletonBoneLayer;

    // data
    private GameObject skeletonGameObject; // Empty object which is the parent of all other game objects of this skeleton
    public KinectWrapper.NuiSkeletonPositionIndex[,] bones { get; private set; } // List of bones that construcs the skeleton ( do not include body, head and hands )
    public GameObject[] bonesObjects { get; private set; } // List of game objects associated to the bones
    public GameObject chest { get; private set; }
    public GameObject handLeft { get; private set; }
    public GameObject handRight { get; private set; }
    public GameObject head { get; private set; }
    public GameObject spine { get; private set; } // this is used only for reference, it does not represent an skeleton body
	private bool visible = true; // Make gameobjects bones visibles or not



    // It initializes the bones and skeleton
    public Skeleton(bool visible = true)
    {
        SkeletonBoneLayer = LayerMask.NameToLayer("SkeletonBone");

        this.skeletonGameObject = new GameObject("Skeleton");
        this.skeletonGameObject.SetActive(false);

        initializeBones();
        initializeChest();
        initializeHead();
        initializeHands();
        initializeSpine();

        setVisible(visible);

    }

	// Activates or desactivates the skeleton gameobjects
	public void setActive(bool active)
    {
        this.skeletonGameObject.SetActive(active);
    }
	
	// Makes all the gameobjects forming the skeleton visibles or not
    public void setVisible(bool b)
    {
        foreach (GameObject bone in bonesObjects) bone.GetComponent<Renderer>().shadowCastingMode = b ? ShadowCastingMode.On : ShadowCastingMode.ShadowsOnly;
        this.chest.GetComponent<Renderer>().shadowCastingMode = b ? ShadowCastingMode.On : ShadowCastingMode.ShadowsOnly;
        this.handLeft.GetComponent<Renderer>().shadowCastingMode = b ? ShadowCastingMode.On : ShadowCastingMode.ShadowsOnly;
        this.handRight.GetComponent<Renderer>().shadowCastingMode = b ? ShadowCastingMode.On : ShadowCastingMode.ShadowsOnly;
        this.head.GetComponent<Renderer>().shadowCastingMode = b ? ShadowCastingMode.On : ShadowCastingMode.ShadowsOnly;

        this.visible = b;
    }

    public bool isVisible()
    {
        return visible;
    }
	
	// Initializes all properties of the head gameobject
    private void initializeHead()
    {
        this.head = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        this.head.SetActive(false);
        this.head.transform.localScale = new Vector3(HEAD_SCALE, HEAD_SCALE, HEAD_SCALE);
        this.head.transform.parent = this.skeletonGameObject.transform;
        this.head.name = "Head";
    }
	
	// Initializes all properties of the chest gameobject
	private void initializeChest()
    {

        this.chest = GameObject.CreatePrimitive(PrimitiveType.Cube);
        this.chest.SetActive(false);
        this.chest.name = "Chest";
        this.chest.layer = SkeletonBoneLayer;
        this.chest.transform.parent = this.skeletonGameObject.transform;
        Rigidbody rb = this.chest.AddComponent<Rigidbody>();
        initializeRigidbody(ref rb);
        this.chest.transform.localScale = new Vector3(1, 1, CHEST_WIDTH);
    }
	
	// Initializes all properties of the spine gameobject. It is an empty gameobject used only for reference
    private void initializeSpine()
    {
        this.spine = new GameObject();
        this.spine.SetActive(false);
        this.spine.transform.parent = this.skeletonGameObject.transform;
        this.spine.name = "Spine";
    }

	// Initializes all properties of both hands gameobjects
    private void initializeHands()
    {
        this.handLeft = initializeHand("hand left");
        this.handRight = initializeHand("hand right");
    }

	// Initialize a single hand
    private GameObject initializeHand(string name)
    {
        GameObject hand = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        hand.transform.localScale = HAND_SCALE;
        hand.SetActive(false);
        hand.transform.parent = this.skeletonGameObject.transform;
        hand.name = name;
        hand.tag = HAND_TAG;
        Rigidbody rb = hand.AddComponent<Rigidbody>();
        initializeRigidbody(ref rb);

        return hand;
    }

    // Create a GameObject that represents the bone for each bone
    private void initializeBones()
    {
		
		// List all the bones to be created. Each bone is represented as a tuple of joints
        this.bones = new KinectWrapper.NuiSkeletonPositionIndex[,] { 
            // Left Arm
            {KinectWrapper.NuiSkeletonPositionIndex.HandLeft, KinectWrapper.NuiSkeletonPositionIndex.WristLeft },
            {KinectWrapper.NuiSkeletonPositionIndex.WristLeft, KinectWrapper.NuiSkeletonPositionIndex.ElbowLeft },
            {KinectWrapper.NuiSkeletonPositionIndex.ElbowLeft, KinectWrapper.NuiSkeletonPositionIndex.ShoulderLeft },
            //{JointType.ShoulderLeft, JointType.SpineShoulder },

            // Right Arm
            {KinectWrapper.NuiSkeletonPositionIndex.HandRight, KinectWrapper.NuiSkeletonPositionIndex.WristRight },
            {KinectWrapper.NuiSkeletonPositionIndex.WristRight, KinectWrapper.NuiSkeletonPositionIndex.ElbowRight },
            {KinectWrapper.NuiSkeletonPositionIndex.ElbowRight, KinectWrapper.NuiSkeletonPositionIndex.ShoulderRight },
            //{JointType.ShoulderRight, JointType.SpineShoulder },

            // Left  Leg
            {KinectWrapper.NuiSkeletonPositionIndex.HipCenter, KinectWrapper.NuiSkeletonPositionIndex.HipLeft },
            {KinectWrapper.NuiSkeletonPositionIndex.HipLeft, KinectWrapper.NuiSkeletonPositionIndex.KneeLeft },
            {KinectWrapper.NuiSkeletonPositionIndex.KneeLeft, KinectWrapper.NuiSkeletonPositionIndex.AnkleLeft },

            // // Right Leg
            {KinectWrapper.NuiSkeletonPositionIndex.HipCenter, KinectWrapper.NuiSkeletonPositionIndex.HipRight },
            {KinectWrapper.NuiSkeletonPositionIndex.HipRight, KinectWrapper.NuiSkeletonPositionIndex.KneeRight },
            {KinectWrapper.NuiSkeletonPositionIndex.KneeRight, KinectWrapper.NuiSkeletonPositionIndex.AnkleRight }

        };

        this.bonesObjects = new GameObject[this.bones.Length / 2];

		// Create and initialize each bone
        for (int i = 0; i < this.bonesObjects.Length; i++)
        {
            this.bonesObjects[i] = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            this.bonesObjects[i].name = String.Format("{0}_{1}", this.bones[i, 0].ToString(), this.bones[i, 1].ToString());
            this.bonesObjects[i].layer = SkeletonBoneLayer;
            this.bonesObjects[i].transform.localScale = new Vector3(BONE_RADIUS, 1, BONE_RADIUS);
            this.bonesObjects[i].transform.parent = this.skeletonGameObject.transform;
            this.bonesObjects[i].SetActive(false);

            // initialize rigidbody
            Rigidbody rb = this.bonesObjects[i].AddComponent<Rigidbody>();
            initializeRigidbody(ref rb);
        }
    }

	// Set the properties of a rigidbody
    private void initializeRigidbody(ref Rigidbody rb)
    {
        rb.useGravity = false;
        rb.isKinematic = true;
        rb.mass = BONE_MASS;
        rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
        rb.drag = BONE_DRAG;
        rb.angularDrag = BONE_ANGULAR_DRAG;
    }


}