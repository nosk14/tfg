﻿using UnityEngine;
using System.Collections.Generic;
using System;

/**
	This class updates all the bones changeing its position and so on.
	It is used to update the skeleton on each frame with the kinect information
**/

public class SkeletonTraker : MonoBehaviour
{
    // Constants
    public const float SCALE_FACTOR = 1f;
    public Transform ORIGIN; // Point representing the (0,0) coordinates

    // Properties
    public Skeleton skeleton { get; private set; }
    public bool visibleSkeleton = false;
    private bool isChestInitialized;
    private Dictionary<int, GameObject> leafs; // A map containing leaf joint and its gameobject
	private uint playerID;
    private KinectManager kinect;

    // Use this for initialization
    void Start()
    {
        this.skeleton = new Skeleton(visibleSkeleton); // Create an skeleton
        this.isChestInitialized = false;
        this.leafs = new Dictionary<int, GameObject>()
            {
                { (int)KinectWrapper.NuiSkeletonPositionIndex.Head, skeleton.head },
                { (int)KinectWrapper.NuiSkeletonPositionIndex.HandLeft, skeleton.handLeft },
                { (int)KinectWrapper.NuiSkeletonPositionIndex.HandRight, skeleton.handRight },
                { (int)KinectWrapper.NuiSkeletonPositionIndex.Spine, skeleton.spine },
            };
    }

    // Update the skeleton on each frame
    void Update()
    {
        this.kinect = KinectManager.Instance;
        if (this.kinect && this.kinect.IsInitialized() && this.kinect.IsUserDetected())
        {
            this.skeleton.setActive(true);
            this.playerID = kinect.GetPlayer1ID();
            updateBones();
            updateChest();
            updateLeafs();
        }
    }

	// Update all leaf bones
    private void updateLeafs()
    {
        foreach(KeyValuePair<int,GameObject> kvp in this.leafs)
        {
            updateLeaf(kvp.Key, kvp.Value);
        }
    }

	
	// Update a single leaf bone
	private void updateLeaf(int joint, GameObject skeletonObject)
    {
        if (kinect.IsJointTracked(this.playerID, joint))
        {
            Vector3 pos = kinect.GetJointPosition(playerID, joint);
            skeletonObject.transform.position = pos + ORIGIN.position;
            skeletonObject.SetActive(true);
        }
        else
        {
            skeletonObject.SetActive(false); // desactivate not tracked bones to avoid errors
        }

    }
	
    // Update all bones of the skeleton
    private void updateBones()
    {

        // For each bone, do the maths and update
        for (int i = 0; i < this.skeleton.bonesObjects.Length; i++)
        {
            int joint1 = (int)skeleton.bones[i, 0];
            int joint2 = (int)skeleton.bones[i, 1];

            if (this.kinect.IsJointTracked(this.playerID, joint1) && this.kinect.IsJointTracked(this.playerID, joint2))
            {
                Vector3 jp1 = getJointWorldPosition(joint1);
                Vector3 jp2 = getJointWorldPosition(joint2);

                updateBone(this.skeleton.bonesObjects[i], jp1, jp2);
            }
            else
            {
                this.skeleton.bonesObjects[i].SetActive(false);
            }

        }
    }

    // Update the transform of a single bone
    private void updateBone(GameObject bone, Vector3 pos1, Vector3 pos2)
    {
        bone.SetActive(true);

		// The center of the bone is the center between the two joints
        Vector3 newPosition = (pos1 + pos2) / 2; 
        bone.transform.position = newPosition;

		// Rotate the bone
        bone.transform.LookAt(pos1);
        bone.transform.Rotate(new Vector3(90, 0, 0));

		// Calc the lenght of the bone
        float distance = Vector3.Distance(pos1, pos2);
        bone.transform.localScale = new Vector3(bone.transform.localScale.x, (distance / 2), bone.transform.localScale.z);
    }

    // Update the body (chest) of skeleton
    private void updateChest()
    {
		// Check if it is necessary to initialize chest
        if (!this.isChestInitialized
            && this.kinect.IsJointTracked(this.playerID, (int)KinectWrapper.NuiSkeletonPositionIndex.HipCenter)
            && this.kinect.IsJointTracked(this.playerID, (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderCenter)
            && this.kinect.IsJointTracked(this.playerID, (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderLeft)
            && this.kinect.IsJointTracked(this.playerID, (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderRight)
            )
        {
            this.initializeChest();
            this.skeleton.chest.SetActive(true);
        }

        Vector3 ref1 = getJointWorldPosition((int)KinectWrapper.NuiSkeletonPositionIndex.HipCenter);
        Vector3 ref2 = getJointWorldPosition((int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderCenter);

        this.skeleton.chest.transform.position = (ref1 + ref2) / 2; // he center of the chest is the point in the middle of the hip and shoulder

        Quaternion orientation = getChestOrientation();
        this.skeleton.chest.transform.rotation = orientation;

    }

	// Sets the height and with of the chest for the first time
    private void initializeChest()
    {
        Vector3 ref1 = getJointWorldPosition((int)KinectWrapper.NuiSkeletonPositionIndex.HipCenter);
        Vector3 ref2 = getJointWorldPosition((int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderCenter);

        float height = (ref1 - ref2).magnitude;

        Vector3 shoulderLeft = getJointWorldPosition((int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderLeft);
        Vector3 shoulderRight = getJointWorldPosition((int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderRight);

        float width = (shoulderLeft - shoulderRight).magnitude;

        this.skeleton.chest.transform.localScale = new Vector3(width, height, this.skeleton.chest.transform.localScale.z);

        this.isChestInitialized = true;
    }

	// Transform a kinct point to a 3Dworld point
    private Vector3 getJointWorldPosition(int jointID)
    {
        return this.kinect.GetJointPosition(this.playerID, jointID) * SCALE_FACTOR + ORIGIN.position;
    }

	// Returns the orientation for the chest gameobject
    private Quaternion getChestOrientation()
    {
		// We can get the orientation of different joints, so lets try
        if (this.kinect.IsJointTracked(this.playerID, (int)KinectWrapper.NuiSkeletonPositionIndex.Spine))
        {
            return this.kinect.GetJointOrientation(this.playerID, (int)KinectWrapper.NuiSkeletonPositionIndex.Spine, false);
        }

        if (this.kinect.IsJointTracked(this.playerID, (int)KinectWrapper.NuiSkeletonPositionIndex.HipCenter))
        {
            return this.kinect.GetJointOrientation(this.playerID, (int)KinectWrapper.NuiSkeletonPositionIndex.HipCenter, false);
        }

        if (this.kinect.IsJointTracked(this.playerID, (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderCenter))
        {
            return this.kinect.GetJointOrientation(this.playerID, (int)KinectWrapper.NuiSkeletonPositionIndex.ShoulderCenter, false);
        }

        return this.kinect.GetJointOrientation(this.playerID, (int)KinectWrapper.NuiSkeletonPositionIndex.Spine, false);

    }

	// Getter 
    public Skeleton getSkeleton()
    {
        return this.skeleton;
    }

}


