﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class MoveAndDestroy : MonoBehaviour {

    public long timeToLive = 1500;
    public float speed = 2f;

    private DateTime initialTime;

	// Update is called once per frame
    void Start()
    {
        initialTime = DateTime.Now;
    }

	void Update () {
        TimeSpan timeSpent = (DateTime.Now - initialTime);
        this.gameObject.transform.Translate(Vector3.up * speed);        

        if ( timeSpent.TotalMilliseconds > timeToLive)
        {
            Destroy(this.gameObject);
        }
	}
}
