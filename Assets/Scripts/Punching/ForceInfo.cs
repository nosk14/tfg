﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ForceInfo : MonoBehaviour
{
    public Canvas canvas;
    public Text maxScoreText;
    private float maxScore = -1f;
    public Text scoreObject;

    public bool showForce = true;

    void onEnable()
    {
        if (maxScore < 0)
        {
            maxScoreText.text = "---";
        }
        else
        {
            maxScoreText.text = scoreToForceString(maxScore);
        }
    }

    void OnCollisionEnter(Collision col)
    {

        if (col.gameObject.tag.Equals(Skeleton.HAND_TAG))
        {
            // Update max score
            //float force = col.relativeVelocity.magnitude;
            float force = col.impulse.magnitude;

            if (force > maxScore)
            {
                maxScore = force;
                maxScoreText.text = scoreToForceString(force);
            }

            // Show score on hit
            if (showForce)
            {
                Text newObject = GameObject.Instantiate(scoreObject);
                newObject.transform.SetParent(canvas.transform, false);
                newObject.text = scoreToForceString(force);
                newObject.transform.position = worldToCanvasSpace(col.contacts[0].point);
            }
        }
    }


    private string scoreToForceString(float score)
    {
        float force = score / Time.fixedDeltaTime;
        return ((int)force).ToString();
    }

    private Vector3 worldToCanvasSpace(Vector3 pos)
    {
        Vector3 screenPos = Camera.main.WorldToViewportPoint(pos);
        screenPos.x *= canvas.GetComponent<RectTransform>().rect.width;
        screenPos.y *= canvas.GetComponent<RectTransform>().rect.height;

        return screenPos;
    }
}
