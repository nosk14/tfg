﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ScoreManager : MonoBehaviour {

    public Canvas canvas;
    public Text scoreObject;

    public Text maxScoreText;
    private int maxScore = -1;

    public PataCojaListener listener;
    public SkeletonTraker skeleton;

    private const int ERROR_RANGE = 1000;
    private const float DESTROY_TIME = 2f; // in seconds

    private bool gestureInProgress = false;
    private Text localScoreText;
    private DateTime gestureStart; 

	// Use this for initialization
	void onEnable () {
        if (maxScore < 0)
        {
            maxScoreText.text = "---";
        }
        else
        {
            maxScoreText.text = ((int)maxScore).ToString();
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (listener.getLastTimeSpanMilis() < ERROR_RANGE) // Gesture in progress
        {
            if (gestureInProgress)
            {
                localScoreText.text = ((int)(DateTime.Now - gestureStart).TotalSeconds).ToString();
            }
            else
            {
                Vector3 pos = worldToCanvasSpace(skeleton.skeleton.head.transform.position + Vector3.up * 0.35f);
                localScoreText = GameObject.Instantiate(scoreObject);
                localScoreText.transform.SetParent(canvas.transform, false);
                localScoreText.transform.position = pos;
                localScoreText.text = "0";
                gestureStart = DateTime.Now;

            }
            gestureInProgress = true;
        }
        else
        {
            if (gestureInProgress)
            {
                int score = (int)(DateTime.Now - gestureStart).TotalSeconds;
                if (score > maxScore)
                {
                    maxScore = score;
                    maxScoreText.text = maxScore.ToString();
                }

            }
            this.gestureInProgress = false;
            if (localScoreText != null)
            {
                localScoreText.gameObject.AddComponent<MoveAndDestroy>();
                localScoreText = null;
            }
        }
	}

    private Vector3 worldToCanvasSpace(Vector3 pos)
    {
        Vector3 screenPos = Camera.main.WorldToViewportPoint(pos);
        screenPos.x *= canvas.GetComponent<RectTransform>().rect.width;
        screenPos.y *= canvas.GetComponent<RectTransform>().rect.height;

        return screenPos;
    }
}
