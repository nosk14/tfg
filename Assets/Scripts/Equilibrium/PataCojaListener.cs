﻿using UnityEngine;
using System.Collections;
using System;

public class PataCojaListener : MonoBehaviour, KinectGestures.GestureListenerInterface
{
    private DateTime? lastTimeGesture;

    public bool GestureCancelled(uint userId, int userIndex, KinectGestures.Gestures gesture, KinectWrapper.NuiSkeletonPositionIndex joint)
    {
        return true;
    }

    public bool GestureCompleted(uint userId, int userIndex, KinectGestures.Gestures gesture, KinectWrapper.NuiSkeletonPositionIndex joint, Vector3 screenPos)
    {
        this.lastTimeGesture = DateTime.Now;
        return true;
    }

    public void GestureInProgress(uint userId, int userIndex, KinectGestures.Gestures gesture, float progress, KinectWrapper.NuiSkeletonPositionIndex joint, Vector3 screenPos)
    {
        this.lastTimeGesture = DateTime.Now;
    }

    public void UserDetected(uint userId, int userIndex)
    {
        KinectManager kinect = KinectManager.Instance; 
        kinect.DetectGesture(userId, KinectGestures.Gestures.PataCoja);
    }

    public void UserLost(uint userId, int userIndex)
    {

    }

    // Getters

    public DateTime getLastTimeGesture()
    {
        if (this.lastTimeGesture == null)
        {
            return DateTime.Now;
        }

        return (DateTime)this.lastTimeGesture;
    }

    public double getLastTimeSpanMilis()
    {
        if (this.lastTimeGesture == null)
        {
            return Double.MaxValue;
        }

        return (DateTime.Now - (DateTime)this.lastTimeGesture).TotalMilliseconds;
    }


}
