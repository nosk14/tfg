﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
	This class manages some options of the game by keyboard inputs.
	secene change, skeleton objects and user image visibility and so
*/

/** DEPRECTADED **/
public class GameManager : MonoBehaviour
{

    public Transform gameParent;
    public List<GameObject> games;

    public SkeletonTraker skeletonTracker;
    public UserImage userImage;

    private GameObject currentGame;


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // Game changers
        if (Input.GetKeyDown(KeyCode.F1))
        {
            changeGame(0);
        }

        else if (Input.GetKeyDown(KeyCode.F2))
        {
            changeGame(1);
        }

        else if (Input.GetKeyDown(KeyCode.F3))
        {

        }

        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        // View skeleton
        else if (Input.GetKeyDown(KeyCode.Q))
        {
            skeletonTracker.skeleton.setVisible(!skeletonTracker.skeleton.isVisible());
        }

        // View user
        else if (Input.GetKeyDown(KeyCode.W))
        {
            userImage.gameObject.SetActive(!userImage.gameObject.activeSelf);
        }

        // View information
        else if (Input.GetKeyDown(KeyCode.I))
        {

        }
    }

    private void changeGame(int i)
    {
        if (currentGame != null) currentGame.SetActive(false);
        currentGame = games[i];
        games[i].SetActive(true);

    }
}
